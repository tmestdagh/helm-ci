FROM alpine:3.7

LABEL org.label-schema.vcs-url="https://gitlab.com/tmestdagh/helm-ci"

ENV KUBECTL_VERSION v1.16.2
ENV HELM_VERSION v3.2.4
ENV PACKAGE_NAME helm-${HELM_VERSION}-linux-amd64.tar.gz

RUN apk --no-cache add \ 
      bash \
      curl \
      git \
      openssh-client \
      jq

# Install Kubectl
RUN curl -SL https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
 && chmod +x /usr/local/bin/kubectl

#Install Helm
RUN curl -SL https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz \
 | tar -xzv --strip-components=1 -C /usr/local/bin linux-amd64/helm

# List versions
RUN kubectl version --client
RUN helm version

RUN helm plugin install https://github.com/databus23/helm-diff
